package ru.t1.avfilippov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.model.ICommand;
import ru.t1.avfilippov.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentListCommand extends AbstractSystemCommand {

    @NotNull
    @Override
    public String getArgument() {
        return "-arg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "show arguments list";
    }

    @NotNull
    @Override
    public String getName() {
        return "arguments";
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (@NotNull final ICommand command : commands) {
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}

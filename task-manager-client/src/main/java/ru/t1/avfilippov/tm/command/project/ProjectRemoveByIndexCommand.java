package ru.t1.avfilippov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.request.ProjectRemoveByIndexRequest;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "remove project by index";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ProjectRemoveByIndexRequest request = new ProjectRemoveByIndexRequest(getToken());
        request.setIndex(index);
        getProjectEndpoint().removeProjectByIndex(request);
    }

}

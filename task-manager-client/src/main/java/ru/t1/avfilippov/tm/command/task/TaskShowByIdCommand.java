package ru.t1.avfilippov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.dto.request.TaskShowByIdRequest;
import ru.t1.avfilippov.tm.dto.response.TaskShowByIdResponse;
import ru.t1.avfilippov.tm.exception.entity.TaskNotFoundException;
import ru.t1.avfilippov.tm.model.Task;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "show task by id";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken());
        request.setTaskId(id);
        @Nullable TaskShowByIdResponse response = getTaskEndpoint().showTaskById(request);
        if (response.getTask()== null) throw new TaskNotFoundException();
        showTask(response.getTask());
    }
}

package ru.t1.avfilippov.tm.repository;


import ru.t1.avfilippov.tm.api.repository.ISessionRepository;
import ru.t1.avfilippov.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

}

